import java.util.Scanner;

public class Main {
    public static void intro(){
        System.out.println(
                   """
                   you are in a world of dragons choose the 
                   left or right cave (1) or (2) 
                   """
        );
    }

    public static int getInput(){
        return new Scanner(System.in).nextInt();
    }

    public static void ending(int choice){
        if (choice == 1){
            System.out.println("you died a horrible slow death...");
            return;
        }
        System.out.println("You found a treasure!");
    }

    public static void main(String[] args) {
        intro();
        ending(getInput());
    }
}
